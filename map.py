import time

from autobahn.twisted.wamp import ApplicationSession
from twisted.internet.defer import inlineCallbacks
from twisted.logger import Logger

from browser.BrowserClientSessionStorage import BrowserClientSessionStorage
from geo import GeoHelper
from pro.ProLocations import ProLocations


class AppSession(ApplicationSession):
    log = Logger()
    ProLocations = None
    geoHelper = None
    SessionStorage = None

    def __init__(self, config):
        ApplicationSession.__init__(self, config)
        self.init()

    def init(self):
        self.SessionStorage = BrowserClientSessionStorage()
        self.ProLocations = ProLocations()

    @inlineCallbacks
    def onJoin(self, details):
        # get pros on initial load.should not be used more then once.
        def setup_sesion(lat, lon, radius):
            session_uuid = self.SessionStorage.init_session(lat, lon, radius)
            return session_uuid

        def get_pros_in_session(uuid, time_period=86400):
            '''
            :param uuid: string - session identifier
            :param time_period: default value = 24h
            :return:
            '''
            session_info = self.SessionStorage.get_session(uuid)
            pros_location_list = self.ProLocations.retrive_all_locations(session_info['lat'], session_info['lon'],
                                                                         session_info['radius'], time_period)
            return pros_location_list

        def get_pro_last_known_locations(pro_id, time_period=86400):
            '''
            :param pro_id: - unique pro id
            :param time_period: default value = 24h
            :return:
            '''
            pro_locations_list = self.ProLocations.retrive_pro_locations(pro_id, time_period)
            return pro_locations_list

        # getting current session info this should be removed, used for debug and demo
        def get_current_session(uuid):
            return self.SessionStorage.get_session(uuid)

        def destroy_session(uuid):
            self.SessionStorage.destroy_session(uuid)

        def publish_pro_coord(lat, lon, pro_id):
            pro_on_map = {
                'lat': lat,
                'lon': lon,
                'id': pro_id,
                'last_seen': time.time()
            }
            self.log.info('publishing pro....lat ' + str(lat) + ' lon ' + str(lon) + ' id ' + str(pro_id))
            self.ProLocations.add_location(pro_on_map['last_seen'], pro_on_map)
            self.publish_pro_updates(pro_on_map)

        yield self.register(get_pros_in_session, 'com.map.get_pros_in_session')
        yield self.register(get_pro_last_known_locations, 'com.map.get_pro_last_known_locations')
        yield self.register(setup_sesion, 'com.map.setup_session')
        yield self.register(publish_pro_coord, 'com.map.publish_pro_coord')
        yield self.register(get_current_session, 'com.map.get_current_session')
        yield self.register(destroy_session, 'com.map.destroy_session')

    def publish_pro_updates(self, pro_on_map: dict):
        active_browser_sessions = self.SessionStorage.get_all()
        for (session_uuid, session_data) in active_browser_sessions:
            # here we got to check if location of pro meets session reqrs
            if GeoHelper.check_the_distance(session_data, pro_on_map) is True:
                browser_event = 'com.map.pro_appeared_' + session_uuid
                self.publish(browser_event, pro_on_map)
