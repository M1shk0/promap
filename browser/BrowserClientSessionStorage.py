import uuid
from geo import GeoHelper


class BrowserClientSessionStorage:

    session_storage = None
    threshold = None

    def __init__(self):
        self.session_storage = []
        self.threshold = 0.1  # 100 meters

    def init_session(self, lat, lon, radius):
        '''
        :param lat: float
        :param lon: float
        :param radius: int
        :return: string
        '''
        session_uuid = self.get_session_by_lat_lon(lat, lon, radius, self.threshold)
        if session_uuid is None:
            session_uuid = self.add_session(lat, lon, radius)

        return session_uuid

    def get_session(self, session_uuid):
        '''
        :param session_uuid:
        :return: dict
        '''
        return dict(self.session_storage)[session_uuid]

    def destroy_session(self, session_uuid):
        '''
        :param session_uuid:
        :return: None
        '''
        for i, (sess_st_uuid, data) in enumerate(self.session_storage):
            if sess_st_uuid == session_uuid:
                del self.session_storage[i]
                break

    def add_session(self, lat, lon, radius):
        '''
        :param lat: float
        :param lon: float
        :param radius: int
        :return: string
        '''
        unique_uuid = uuid.uuid4().__str__()
        self.session_storage.append(
            (unique_uuid, {"lat": lat, "lon": lon, "radius": radius}))
        return unique_uuid

    def get_session_by_lat_lon(self, lat, lon, radius, threshold=None):
        '''
        :param lat: float
        :param lon: float
        :param radius: int
        :param threshold: int
        :return: res: string
        '''
        res = None
        for i, (sess_st_uuid, data) in enumerate(self.session_storage):
            if threshold is None and data['lat'] == lat and data['lon'] == lon and data['radius'] == radius:
                res = sess_st_uuid
                break
            elif (threshold is not None and GeoHelper.check_the_distance(data, {'lat': lat, 'lon': lon}) < threshold and
                          data['radius'] == radius):
                res = sess_st_uuid
                break
        return res

    def get_all(self):
        '''
        :return: list
        '''
        return self.session_storage
