local result = {}
local decoded_location_data ={}
local location_set_key = KEYS[1]
local pro_id = ARGV[1]
local right_border_time = ARGV[2]
local left_border_time = ARGV[3]


local data = redis.call('ZREVRANGEBYSCORE',location_set_key,right_border_time,left_border_time)

if data ~= false then
    for i,location_data  in ipairs(data) do
        decoded_location_data = cjson.decode(location_data)
        if decoded_location_data['id'] == pro_id then
            result[i] = decoded_location_data
        end
    end
end

return cmsgpack.pack(result)
