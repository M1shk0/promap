local result = {}
local decoded_location_data = {}
local location_set_key = KEYS[1]
local lat = ARGV[1]
local lon = ARGV[2]
local radius = tonumber(ARGV[3])
local right_border_time = ARGV[4]
local left_border_time = ARGV[5]


local function geo_distance(lat1, lon1, lat2, lon2)
    if lat1 == nil or lon1 == nil or lat2 == nil or lon2 == nil then
        return nil
    end
    local dlat = math.rad(lat2 - lat1)
    local dlon = math.rad(lon2 - lon1)
    local sin_dlat = math.sin(dlat / 2)
    local sin_dlon = math.sin(dlon / 2)
    local a = sin_dlat * sin_dlat + math.cos(math.rad(lat1)) * math.cos(math.rad(lat2)) * sin_dlon * sin_dlon
    local c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    local d = 6378 * c
    return d
end

local data = redis.call('ZREVRANGEBYSCORE', location_set_key, right_border_time, left_border_time)

if data ~= false then
    for i, location_data in ipairs(data) do
        decoded_location_data = cjson.decode(location_data)
        if not result[decoded_location_data['id']] and geo_distance(decoded_location_data['lat'], decoded_location_data['lon'], lat, lon) < radius then
            result[decoded_location_data['id']] = decoded_location_data
        end
    end
end

return cmsgpack.pack(result)

