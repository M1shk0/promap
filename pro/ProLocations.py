import json
import os
import time
import msgpack
import redis


class ProLocations:
    storage = None
    LUA_SCRIPTS_LOCATION = None
    LOCATION_SET_KEY = 'PRO_LOCATIONS'

    def __init__(self, config={}):
        self.storage = redis.StrictRedis(host='127.0.0.1', port=6379, db=0)
        self.LUA_SCRIPTS_LOCATION = os.path.dirname(os.path.realpath(__file__)) + '/lua/'

    '''
    adding locations to redis sorted set, using timestamp as a score
    '''

    def add_location(self, timestamp: float, pro_on_map: dict):
        return self.storage.zadd(self.LOCATION_SET_KEY, timestamp, json.dumps(pro_on_map))

    def remove_all_locations(self, pro_id=None):
        if pro_id is None:
            self.storage.delete(self.LOCATION_SET_KEY)
        else:
            # eval removal to filter by pro_id
            self.storage.eval('')

    def retrive_pro_locations(self, pro_id, time_period=None):
        '''
        time_period  - seconds
        '''
        script_pointer = open(self.LUA_SCRIPTS_LOCATION + 'retrive_pro_locations.lua', 'r')
        script_string = script_pointer.read()
        script_pointer.close()

        current_time = time.time()
        left_border_time = current_time - time_period
        return msgpack.unpackb(self.storage.eval(script_string, 1, self.LOCATION_SET_KEY, pro_id, int(current_time),
                                 int(left_border_time)), encoding='utf-8')

    def retrive_all_locations(self, lat, lon, radius, time_period=3600):
        '''
        time_period  - seconds
        '''
        script_pointer = open(self.LUA_SCRIPTS_LOCATION + 'retrive_all_locations.lua', 'r')
        script_string = script_pointer.read()
        script_pointer.close()

        current_time = time.time()
        left_border_time = current_time - time_period
        return msgpack.unpackb(self.storage.eval(script_string, 1, self.LOCATION_SET_KEY, lat, lon, radius, current_time,
                                 left_border_time), encoding='utf-8')
