from math import radians, cos, sin, asin, sqrt


def check_the_distance(session_data: dict, pro_data: dict):
    res = False
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, [session_data['lon'], session_data['lat'], pro_data['lon'], pro_data['lat']])
    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * asin(sqrt(a))
    #3963 for miles
    km = 6367 * c
    if km < ((session_data['radius']) / 1000):
        res = True

    return res
