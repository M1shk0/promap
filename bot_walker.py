#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import random
import time

import geopy
from autobahn.twisted.wamp import ApplicationSession, ApplicationRunner
from autobahn.twisted.websocket import WebSocketClientProtocol
from geopy.distance import VincentyDistance
from twisted.internet.defer import inlineCallbacks


def do_step(from_coord, step_dist, angle=0):
    origin = geopy.Point(from_coord[0], from_coord[1])
    angle = random.randrange(20, 270, 5)

    destination = VincentyDistance(kilometers=step_dist).destination(origin, angle)
    return destination.latitude, destination.longitude, random.randint(0, 7) / 10


class Component(ApplicationSession, WebSocketClientProtocol):
    @inlineCallbacks
    def onJoin(self, details):
        from_coord = None
        start_coord = (start_lat, start_lon, 0)
        distance_m = distance * 1000
        # km/s
        step_dist = float(speed / 3600)
        angle = start_angle
        i = 0
        while distance_m > 0:

            i += 1

            if from_coord is None:
                from_coord = start_coord
            else:
                from_coord = do_step(from_coord, step_dist, angle)

            yield self.call(u'com.map.publish_pro_coord', from_coord[0], from_coord[1], pro_id)
            time.sleep(1)
            distance_m -= step_dist


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='the walking bot.')
    parser.add_argument('-d', '--distance')
    parser.add_argument('-s', '--speed')
    parser.add_argument('-u', '--pro_id')
    parser.add_argument('-la', '--lat')
    parser.add_argument('-lo', '--lon')
    parser.add_argument('-a', '--angle')

    args = parser.parse_args()
    pro_id = str(args.pro_id)
    distance = float(args.distance)
    speed = float(args.speed)
    start_lat = float(args.lat)
    start_lon = float(args.lon)
    start_angle = float(args.angle)

    runner = ApplicationRunner(u"ws://127.0.0.1:8080/ws",
                               u"promap",
                               )
    runner.run(Component)
